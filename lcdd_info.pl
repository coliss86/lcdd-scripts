#!/usr/bin/perl -w

use lib '.';
use strict;
use warnings;
use IO::LCDproc;

if ((scalar(@ARGV) != 1 && scalar(@ARGV) != 2) || $ARGV[0] eq "-help" || $ARGV[0] eq "--help") {
    print_usage();
}

my $client  = IO::LCDproc::Client->new(name => "MYNAME", port => 13666 );
my $screen  = IO::LCDproc::Screen->new(name => "screen");
my $title   = IO::LCDproc::Widget->new(
    name => "title", type => "title"
);
my $info   = IO::LCDproc::Widget->new(
    name => "first", align => "center", type => "string", xPos => 1, yPos => 3
);

$client->add( $screen );
$screen->add( $title, $info );
$client->connect() or die "cannot connect: $!";
$client->initialize();
$screen->set_prio("alert");

$title->set( data => "Information" );
$info->set( data => $ARGV[0] );

$client->flushAnswers();

my $sleep = $ARGV[1];
if (!defined $sleep || $sleep == 0) {
    $sleep = 20;
}
sleep $sleep;

$client->flushAnswers();
$client->{lcd}->close();

exit(0);

#####################
# Print help message
# @param $program_name
sub print_usage {
    print "$0 <message> [time]\n";
    exit (1);
}

