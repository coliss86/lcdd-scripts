#!/usr/bin/perl -w
# show temperature on lcdd

use lib '.';
use strict;
use warnings;
use IO::LCDproc;
use Getopt::Long;
use POSIX qw(setsid);

# influxdb data source
my $table = "ext.nord";
my $database = "temperature";
# default port LCDd
my $port = 13666;
# sleep
my $sleep = 5;
my $help = 0;
# debug
my $debug = 0;

GetOptions("port=i" => \$port,
        "sleep=s" => \$sleep,
        "debug!" => \$debug,
        "help" => \$help) or print_usage();

if ($help) {
    print_usage()
}

my $min = 100;
my $max = -100;
my $current = 0;

if (!$debug) {
    &daemonize;
}

############# DATA #########################
$current = get_data("median");
$min = get_data("min");
$max = get_data("max");

$current = sprintf "%.1f", $current;
$min = sprintf "%.1f", $min;
$max = sprintf "%.1f", $max;

log_debug("current : $current °C");
log_debug("    min : $min °C");
log_debug("    max : $max °C");

############# LCDD #########################

my $client = IO::LCDproc::Client->new(name => "temperature", port => $port );
my $screen = IO::LCDproc::Screen->new(name => "tempK", heartbeat => "off");
my $d1 = IO::LCDproc::Widget->new(name => "d1", type => "num");
my $d2 = IO::LCDproc::Widget->new(name => "d2", type => "num");
my $d3 = IO::LCDproc::Widget->new(name => "d3", type => "num");
my $sign = IO::LCDproc::Widget->new(name => "sign", type => "string");
my $dot = IO::LCDproc::Widget->new(name => "dot", type => "string");
my $minv = IO::LCDproc::Widget->new(name => "minv", type => "string", xPos => 14, yPos => 2 );
my $maxv = IO::LCDproc::Widget->new(name => "maxv", type => "string", xPos => 14, yPos => 3 );
my @ds = ($d1, $d2, $d3);

$client->add( $screen );
$screen->add( $sign, $dot, $d1, $d2, $d3, $minv, $maxv );
$client->connect() or die "cannot connect: $!";
$client->initialize();
$screen->set_prio("alert");

my @chars = split("", $current);

my $pos = 3;
my $dpos = 0;

# clear other char, 11 is an illegal char, it will be replaced by a space
for (; $dpos < scalar(@ds); $dpos++) {
    $ds[$dpos]->set( data => "11", yPos => $pos, xPos => 0);
    $pos += 3; 
}

$pos = 3;
$dpos = 0;
foreach my $char (@chars) {
    if ($char =~ /^\d$/) {
        $ds[$dpos++]->set( data => "$char", yPos => $pos, xPos => 0);
        $pos += 3; 
    } elsif ($char eq "-") {
        $sign->set( data => "_", xPos => 1, yPos => 2);
    } elsif ($char eq ".") {
# note pour mon futur moi : il n'est pas possible de définir des icones personnalisées sans recompiler le driver
        $dot->set( data => ".", xPos => $pos , yPos => 4);
        $pos += 1; 
    }
}

$minv->set( data => "- $min" );
$maxv->set( data => "+ $max" );

$client->flushAnswers();

if ($sleep =~ s/m$//) {
    $sleep = $sleep * 60;
} elsif ($sleep =~ s/h$//) {
    $sleep = $sleep * 60 * 60;
}

sleep $sleep;

$client->flushAnswers();
$client->{lcd}->close();

exit(0);

#####################
# Print help message
# @param $program_name
sub print_usage {
    print "$0 [--sleep|-s 30m] [--debug] [--port 13666]\n";
    exit (1);
}

#####################
# print the message to stdout
# @param $msg : message to display
sub log_debug {
    my $msg = shift;
    if ($debug) {
        print "$msg\n";
    }
}

#####################
# print the message to stdout
# @param $func : function to execute
sub get_data {
    my $func = shift;
    my $line;
    my $res=0;
    my $cmd = "influx -database $database -execute 'SELECT $func(val) FROM \"$table\" where time > now() - 1h;' -format=json | jq \".results[].series[].values[0][1]\""; 
    open(DATA,"$cmd |") or die("command failed : '$cmd' : $!");
    while ($line = <DATA>) {
        chomp($line);
        log_debug($line);
        next if ($line =~ /^\s*$/);
        $res = $line;
    }
    close(DATA) or die("command failed : '$cmd' : $!");
    return $res;
}

#####################
# daemonize my self
sub daemonize {
    chdir '/' or die "Can’t chdir to /: $!";
    open STDIN, '/dev/null' or die "Can’t read /dev/null: $!";
    open STDOUT, '>>/dev/null' or die "Can’t write to /dev/null: $!";
    open STDERR, '>>/dev/null' or die "Can’t write to /dev/null: $!";
    defined(my $pid = fork) or die "Can’t fork: $!";
    exit if $pid;
    setsid or die "Can’t start a new session: $!";
    umask 0;
}

